# Gsearch

This is a fun coding challenge for Appolo ssc.
deployed here -> http://mustapha-aouas.com/Apollo/

## Framework / Library
- Angular
- bootstrap
- animate.css
- css-animator

## Configurtion
- Front : Own API adress in 'the app/servers/api.service.ts'
- Back : IGDB API Key in 'API/index.php'