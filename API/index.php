<?php

	header("Access-Control-Allow-Origin: *");

	if ( ! empty($_GET['r']) )
	{
		//API target
		$api = "https://api-2445582011268.apicast.io/";

		// Getting the input
		$input = $_GET['r'];
		 
		// Parsing
		$requestStr = str_replace("%Q;" , "?", $input);
		$requestStr = str_replace("%A;" , "&", $requestStr);

		// Create a stream
		$opts = array(
		  'http'=>array(
		    'method'=>"GET",
		    'header'=>		"Accept: application/json\r\n".
					  		"user-key: 769b203a5b7d2ddd16d1732e19012a68\r\n"
		  )
		);
		$context = stream_context_create($opts);

		// Open the file using the HTTP headers set above
		$file = file_get_contents($api . $requestStr, false, $context);

		// Display output
		echo $file;
	}

?>