import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AnimationService } from 'css-animator';
import { Routes, RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SearchComponent } from './search/search.component';
import { FormComponent } from './search/form/form.component';
import { ListComponent } from './search/list/list.component';
import { ItemComponent } from './search/list/item/item.component';
import { NewsComponent } from './news/news.component';
import { HomeComponent } from './home/home.component';
import { EsterEggComponent } from './ester-egg/ester-egg.component';
import { ApiService } from './services/api.service';
import { LoaderComponent } from './loader/loader.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'ester-egg', component: EsterEggComponent },
  { path: 'news', component: NewsComponent },
  { path: 'search', component: SearchComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SearchComponent,
    FormComponent,
    ListComponent,
    ItemComponent,
    NewsComponent,
    HomeComponent,
    EsterEggComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AnimationService,
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
