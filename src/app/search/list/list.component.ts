import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ElementRef,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import { ApiService } from '../../services/api.service';
import { AnimationService } from 'css-animator';
import { IForm } from '../../models/IForm.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit, AfterViewInit {
  @Input() from: string = '';
  @ViewChild('divRef') thisElement: ElementRef;
  @Output('page') pageEmitter = new EventEmitter<number>();
  games: any[] = [];
  requestStatus: string = 'wait';
  condition: string;
  conditionNews: string;

  // Form
  form: IForm = { page: 0, numElementToDisplay: 3, platform: '', search: '', gameMode: '', orderBy: 'rating:desc' };

  constructor(private apiService: ApiService, private animationService: AnimationService) { }

  ngOnInit() { }

  ngAfterViewInit() {
    this.getContent();
  }

  doSearch(form: IForm) {
    this.form = form;
    this.getContent();
  }

  getContent(condition: string = null) {
    this.games = [];
    this.requestStatus = 'wait'; // show loader when waiting
    // initialisation of the request dependin on : called from news page or search page
    if (this.from != 'news') {
      this.condition = "games/%Q;fields=*" +
        this.getSearch() + this.getPlatform() + this.getOrgerBy() + this.getGameMode() +
        "%A;limit=" + this.form.numElementToDisplay +
        "%A;offset=" + Number(this.form.numElementToDisplay * this.form.page);
    } else {
      this.condition = "games/%Q;fields=*%A;order=first_release_date:desc%A;filter[first_release_date][lte]=" +
        this.getTimeDate() + "%A;limit=6%A;offset=" + Number(9 * this.form.page);
    }
    // remove useless spaces entred by the user
    condition = this.condition.split(' ').join('');
    // Call the service
    this.apiService.getGames(condition).subscribe(games => {
      games.map(game => { this.games.push(game); });
      if (this.games.length < 1)
        this.games.push({ id: 0, name: 'No game found' });
      // hide the loader
      this.requestStatus = 'done';
      this.animationService.builder().setType('bounceInUp').show(this.thisElement.nativeElement);
    });
  }

  next() {
    this.form.page++;
    this.pageEmitter.emit(this.form.page);
    this.getContent();
  }

  previous() {
    this.form.page--;
    this.form.page = Math.max(0, this.form.page);
    this.pageEmitter.emit(this.form.page);
    this.getContent();
  }

  getTimeDate() {
    let now = new Date();
    return (now.getFullYear() + '-' + now.getMonth() + '-' + now.getDate());
  }

  getSearch() {
    return (this.form.search == '' ? '' : '%A;search=' + this.form.search);
  }

  getPlatform() {
    return (this.form.platform == '' ? '' : '%A;filter[release_dates.platform][eq]=' + this.form.platform);
  }

  getGameMode() {
    return (this.form.gameMode == '' ? '' : '%A;filter[game_modes][eq]=' + this.form.gameMode);
  }

  getOrgerBy() {
    return (this.form.orderBy == '' ? '%A;order=rating:desc' : '%A;order=' + this.form.orderBy);
  }

}
