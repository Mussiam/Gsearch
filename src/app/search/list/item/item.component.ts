import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { AnimationService } from 'css-animator';
import { ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @Input() game;
  @ViewChild('divRef') thisElement: ElementRef;

  constructor(private animationService: AnimationService) { }

  ngOnInit() {
    //this.animationService.builder().setType('fadeIn').show(this.thisElement.nativeElement);
  }

  dateConverter(dateStr: string) {
    let date = new Date(dateStr);
    let ret = date.toLocaleDateString();
    if (ret === 'Invalid Date')
      ret = 'N/A';
    return ret;
  }

  getImg() {
    let img: string;
    if (this.game.cover != null)
      img = "https://igdb.spacechop.com/igdb/image/upload/t_cover_big/" + this.game.cover.cloudinary_id;

    let ret = this.game.cover != null ? img :
      (this.game.screenshots != null ? this.game.screenshots[0].url : 'assets/logo.png');

    return ret;
  }

}
