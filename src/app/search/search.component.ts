import { Component, OnInit, ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';
import { AnimationService } from 'css-animator';
import { IForm } from '../models/IForm.model';
import { ListComponent } from './list/list.component';
import { FormComponent } from './form/form.component';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  @ViewChild('searchRef') thisElement: ElementRef;
  @ViewChild(ListComponent) list: ListComponent;
  @ViewChild(FormComponent) form: FormComponent;

  constructor(private animationService: AnimationService) {
  }

  ngOnInit() {
    //this.animationService.builder().setType('lightSpeedIn').show(this.thisElement.nativeElement);
  }

  onSearch(form: IForm) {
    console.log('onSearch : ' + Object.values(form));
    this.list.doSearch(form);
  }

  onNavigation(page: number) {
    console.log('onNavigation: ' + page);
    this.form.doUpdatePage(page);
  }

}
