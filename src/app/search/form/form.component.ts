import { Component, OnInit } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { IForm } from '../../models/IForm.model';
import { Output } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @Output('search') searchEmitter = new EventEmitter<IForm>();
  form: IForm = this.getInitialForm();

  constructor() { }

  ngOnInit() {
  }

  getInitialForm() {
    return { page: 0, numElementToDisplay: 3, platform: '', search: '', gameMode: '', orderBy: 'rating:desc' };
  }

  onSearch(arg: any = null, resetPage: boolean = true) {
    if (resetPage)
      this.form.page = 0;
    console.log('emitted: ' + Object.values(this.form));
    this.searchEmitter.emit(this.form);
  }

  onClear() {
    this.form = this.getInitialForm();
    this.onSearch();
  }

  doUpdatePage(i: number) {
    this.form.page = i;
  }

}
