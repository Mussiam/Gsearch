import { Component, OnInit, ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';
import { AnimationService } from 'css-animator';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  @ViewChild('newsRef') thisElement: ElementRef;

  constructor(private animationService: AnimationService) {
  }

  ngOnInit() {
    //this.animationService.builder().setType('lightSpeedIn').show(this.thisElement.nativeElement);
  }

}
