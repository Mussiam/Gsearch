import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ElementRef } from '@angular/core';
import { AnimationService } from 'css-animator';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild('homeRef') thisElement: ElementRef;

  constructor(private router: Router, private animationService: AnimationService) { }

  ngOnInit() {
    this.animationService.builder().setType('flipInX').show(this.thisElement.nativeElement);
  }

  onContinue() {
    this.router.navigate(['/search']);
  }

}
