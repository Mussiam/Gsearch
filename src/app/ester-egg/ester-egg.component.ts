import { Component, OnInit, ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';
import { AnimationService } from 'css-animator';

@Component({
  selector: 'app-ester-egg',
  templateUrl: './ester-egg.component.html',
  styleUrls: ['./ester-egg.component.css']
})
export class EsterEggComponent implements OnInit {
  inputs: string = '';
  @ViewChild('divRef') thisElement: ElementRef;
  resolved = false;

  constructor(private animationService: AnimationService) { }

  ngOnInit() {
    this.animationService.builder().setType('lightSpeedIn').show(this.thisElement.nativeElement);
  }

  onAddInput(str: string) {
    this.inputs += str.trim();
    this.verification();
    console.log(this.inputs);
  }

  verification() {
    if (this.inputs.length == 11 && this.inputs == '11223434bas') {
      //alert('Wow you know the konami code!');
      this.animationService.builder().setType('tada').show(this.thisElement.nativeElement);
      this.inputs = '';
      this.resolved = true;
    } else if (this.inputs.length >= 11) {
      this.animationService.builder().setType('wobble').show(this.thisElement.nativeElement);
      this.inputs = '';
    }

  }

}


