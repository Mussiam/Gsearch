export interface IForm {
    page: number;
    numElementToDisplay: number;
    platform: string;
    search: string;
    gameMode: string;
    orderBy: string;
}