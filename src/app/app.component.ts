import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  selectedPage = 'search';
  title = 'Gsearch';

  constructor() { }

  onNavChange(page: string) {
    this.selectedPage = page;
  }
}
