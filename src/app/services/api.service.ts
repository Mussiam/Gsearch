import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/Rx';

@Injectable()
export class ApiService {
    baseUrl: String;
    http: any;

    constructor(http: Http) {
        this.http = http;
        this.baseUrl = "http://mustapha-aouas.com/Apollo/API/?r=";
    }

    getGames(condition: string, arg2 = null) {
        return this.http.get(this.baseUrl + condition).map(res => res.json());
    }
}
